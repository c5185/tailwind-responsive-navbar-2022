module.exports = {
  // content: ["./public/**/*.{html,js}"],
  mode:'jit',
  purge:[
    './public/index.html'
  ],
  darkMode:false,
  theme: {
  
    extend: {
     
      colors:{
        "theme-black":"#272727",
        "theme-green-light":"#fbfefb",
      },
      height:{
        'screen-75':'90vh',
      },
      fontFamily:{
        'open-sans':['Open Sans', 'sans-serif']
      }
    },
  },
  plugins: [],
}
